# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '~> 3.0'

gem 'rails', '~> 6.1'

gem 'bcrypt'
gem 'bootsnap', '>= 1.1.0', require: false
gem 'graphql'
gem 'haml'
gem 'haml-rails'
gem 'imperator', github: 'karmajunkie/imperator'
gem 'puma'
gem 'pundit'
gem 'sassc-rails'
gem 'sqlite3'
gem 'turbolinks'
gem 'uglifier'
gem 'validates_email_format_of'

group :development, :test do
  gem 'amazing_print'
  gem 'binding_of_caller'
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  gem 'graphiql-rails'
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'm', '~> 1.5.0'
  gem 'minitest-reporters', github: 'kern/minitest-reporters'
  gem 'policy-assertions'
  gem 'pry-byebug'
  gem 'pry-rails'
  gem 'simplecov', github: 'colszowka/simplecov', require: false
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

group :development do
  gem 'annotate', require: false
  # gem 'brakeman'
  gem 'guard', github: 'guard/guard'
  # gem 'guard-brakeman'
  gem 'guard-minitest'
  gem 'guard-rubocop'
  gem 'guard-shell'
  gem 'guard-spring'
  gem 'rails-erd'
  gem 'rubocop'
  gem 'rubocop-performance'
  gem 'rubocop-rails'
  gem 'web-console'
end

group :test do
  gem 'capybara'
  gem 'rails-controller-testing'
  gem 'selenium-webdriver'
  # gem 'webdrivers'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
