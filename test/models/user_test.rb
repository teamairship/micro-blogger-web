# frozen_string_literal: true

# == Schema Information
#
# Table name: users
#
#  id              :integer          not null, primary key
#  display_name    :string           not null
#  email           :string           not null
#  password_digest :string           not null
#  role            :integer          default("author"), not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_users_on_email  (email) UNIQUE
#
require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test "higer roles can act as lower roles" do
    assert users(:admin).acts_as_admin?
    assert users(:admin).acts_as_author?

    assert users(:author).acts_as_author?
  end

  test "lower roles can NOT act as higher roles" do
    assert_not users(:author).acts_as_admin?
  end
end
