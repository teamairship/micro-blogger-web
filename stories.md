# MicroBlogger

A micro blogging platform

* Guests can view published content and author list
* Authors can edit their profile & blogs
* Admins can view the list of authors and edit any blog

## Tasks

### Header/.Menu bar

Create a header bar with a horizontal row of simple links. These links should be positioned at the top of every page, within the main column. The home link should be on the left side, and all other links should be on the right. Be sure the include a comfortable spacing so that users can easily read and click/tap them.

The link list should contain:

* A `home` link navigating back to the default root page
* Conditional login/logout links
* All users should have a link to edit their user info
* Admins should have a link to users#index

### User Bios

As an author, I want to be able to write a short bio that is displayed at the bottom of my postings and my profile page.

### BUG: Publishing

It has been reported that authors are no longer able to publish their blogs out of draft mode.

### Author Titles

As a user, I want to be able to click on an authors name and see their profile page with a list of their published titles that link to that article.

### Categories

_discuss a plan and estimation of effort for this story_

As an author, I would like to be able to assign a category to my posting from an existing list of options.
As an admin, I can manage the list of categories available for the authors to select from.

### Moderation

_discuss a plan and estimation of effort for this story_

As an admin, I want to be able to put a blog in a state of moderation that also unpublishes it.

Once a blog post has this state, the author must edit and submit for another review before an admin will republish.

### Registration

_discuss a plan and estimation of effort for this story_

As a guest, I should be able to register so that I may start producing content.

### GraphQL

A graphql server is running in the Rails application and you can test queries out in your browser at /graphiql.

We currently have a graphql query setup for users.

Update the graphql Blog type to include published_date and user.

Add a graphql query for blogs.

